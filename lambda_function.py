import json
import boto3
from zeep import Client
from zeep.wsse.username import UsernameToken
from datetime import datetime, timedelta, time
import sys
import pandas as pd
import time as timer

from util.general import date2name, name2date
from util.aws_s3 import upload_to_s3, connect_s3, get_secret

start = timer.time()
s3_client = boto3.client('s3')
s3_resource = connect_s3()
s3_bucket = 'ev-modeling-platform-prod'
s3_location = '1_RawData/Charging_ChargePoint/'
s3_meta_codes = '0_ReferenceData/MetaDataForCode/ChargePoint_postalCodes.json'

secret_name = 'ChargePoint_API'
secret_region = 'eu-west-2'


def extract_session_info(session):
    zip0 = session.postalCode.split(' ')[0]
    zip1 = session.postalCode.split(' ')[1][0]
    zip_sector = zip0 + ' ' + zip1
    info = {
    'stationID': session.stationID,
    'sessionID': session.sessionID,
    'Energy': session.Energy,
    'startTime': session.startTime.replace(tzinfo=None),
    'endTime': session.endTime.replace(tzinfo=None),
    'userID': session.userID,
    'ZipSector': zip_sector}
    return info


def get_sessions(client, postalCode, startTime, endTime):
    SearchQuery = {
            'fromTimeStamp': startTime,
            'toTimeStamp': endTime,
            'postalCode': postalCode}

    retries = 1
    wait_in_sec = 60
    success = False

    while not success:
        try:
            data = client.service.getChargingSessionData(SearchQuery)
            success = True
        except Exception as e:
            print(f"ERROR & RETRY: Couldn't access API. Waiting {wait_in_sec} sec before retrying. ")
            sys.stdout.flush()
            timer.sleep(wait_in_sec)

            if retries >= 10:
                print(f"FINAL ERROR: Aborting job due to timeout of API.")
                break

            retries += 1

    return data.ChargingSessionData


def main():
    # --------------------------------------------------------------- API SETUP
    # insert your organization's username and password below
    secret = get_secret(secret_name=secret_name, region_name=secret_region)
    username = secret['username']
    password = secret['password']

    # connection to client, set wsse auth
    wsdl_url = secret['wsdl_url']
    print('INFO: got secrets ', wsdl_url,
          '\n elapsed time (minutes): ', (timer.time() - start) / 60)
    client = Client(wsdl_url, wsse=UsernameToken(username, password))

    # ---------------------------------------------------------- DOWNLOAD SETUP
    # get postalCodes
    meta_bytes = s3_client.get_object(Bucket=s3_bucket,
                      Key=s3_meta_codes)
    file_content = meta_bytes['Body'].read().decode('utf-8')
    codes_json = json.loads(file_content)
    postalCodes = codes_json['codes']

    print('INFO: zip codes to loop through: ', len(postalCodes),
          '\n elapsed time (minutes): ', (timer.time()-start)/60)

    # get dates for Download
    result = s3_client.list_objects(Bucket=s3_bucket, Prefix=s3_location,
                                    Delimiter='/')
    if result.get('CommonPrefixes') is None:
        print('INFO: Did not find any historical data. ',
              'Start with download one year ago. ',
              '\n elapsed time (minutes): ', (timer.time() - start) / 60)
        last_date = datetime.combine(datetime.today(), time.min) \
                    - timedelta(days=365, seconds=1)
    else:
        list = []
        for o in result.get('CommonPrefixes'):
            element = o.get('Prefix').split('/')[-2]
            list.append(element)
        list.sort()
        last_name = list[-1]
        last_date = name2date(last_name, format='YYYY_DXXX')
        if last_date.date() == datetime.today().date() - timedelta(days=1):
            print('INFO: Data is up to date. No need to download ',
                  '\n elapsed time (minutes): ', (timer.time() - start) / 60)
            return {'message': 'Already up to date.'}

    total_startTime = last_date + timedelta(seconds=1)
    total_endTime = datetime.combine(datetime.today(), time.min)

    startTimes_list = pd.date_range(start=total_startTime.date(),
                                     end=total_endTime.date()).to_pydatetime().tolist()

    # reverse list to use list.pop (which takes last element from list, but we
    # need first date, hence have to reverse list first
    startTimes_list = startTimes_list[::-1]

    print('INFO: start download loop. Missing days: ', len(startTimes_list))

    while startTimes_list and ((timer.time() - start)/(60*60) < 20):
        startTime = startTimes_list.pop()
        endTime = startTime + timedelta(hours=23, minutes=59, seconds=59)
        print('INFO: start download for ', startTime,
              '\n elapsed time (minutes): ', (timer.time() - start) / 60)
        # ------------------------------------------------------------ API requests
        all_sessions = []

        postalCodes = postalCodes
        for postalCode in postalCodes:
            day_sessions = get_sessions(client, postalCode, startTime, endTime)
            all_sessions += day_sessions

        # format API responses to DataFrame

        print('INFO: done with download, next: formatting',
              '\n elapsed time (minutes): ', (timer.time() - start) / 60)
        sessions = pd.DataFrame()
        for session in all_sessions:
            session_info = extract_session_info(session)
            sessions = sessions.append(pd.Series(session_info),
                                             ignore_index=True)
        sessions = sessions.drop_duplicates(ignore_index=True)

        print('INFO: done with formatting, next: upload',
              '\n elapsed time (minutes): ', (timer.time() - start) / 60)
        # ------------------------------------------------------------ UPLOAD TO S3
        date_folder = date2name(endTime, format='YYYY_DXXX')
        folder = '1_RawData/Charging_ChargePoint/' + date_folder + '/'

        print('INFO: upload to folder: ', folder)
        print('MESSAGE: Charging Sessions on {}: {}'
            .format(last_date.date(), sessions.shape[0]))

        uploadByteStream = bytes(sessions.to_csv().encode('UTF-8'))
        file_name = 'sessions.csv'
        key = folder + file_name
        upload_to_s3(s3_resource, s3_bucket=s3_bucket, s3_location=key,
                     body=uploadByteStream)

    if startTimes_list:
        message = 'Timelimit. {} downloads left.'.format(len(startTimes_list))
    else:
        message = 'Data up to date.'

    print('INFO: End download loop. - ', message)
    return {'message': message}


if __name__ == '__main__':
    main()
