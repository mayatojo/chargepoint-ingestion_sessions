from datetime import datetime, timedelta


# -------------------------------------- DATE TO FOLDER YYYY_WEEK/MONTH/QUARTER
def date2quarter(date):
    return (date.month-1)//3 +1


def date2name(date, format='YYYY_WXX'):
    """date: datetime.date e.g. datetime.now().date()"""
    if format=='YYYY_DXXX':
        year = str(date.year)
        day = date.strftime('%j')
        name = year + '_D' + day
    elif format=='YYYY_WXX':
        year = str(date.year)
        week = str(date.isocalendar()[1]).zfill(2)
        name = year + '_W' + week
    elif format=='YYYY_MXX':
        year = str(date.year)
        month = str(date.month).zfill(2)
        name = year + '_M' + month
    elif format=='YYYY_QX':
        year = str(date.year)
        quarter = str(date2quarter(date))
        name = year + '_Q' + quarter
    else:
        print('unknown format, return date as string')
        return str(date)
    return name


def name2date(name, format):
    if format == 'YYYY_DXXX':
        year = int(name[:4])
        days = int(name[-3:])
        date = datetime(year, 1, 1, 23, 59, 59) + timedelta(days - 1)
    else:
        print('unknown format, return timestamp of *now*')
        return datetime.now().replace(microsecond=0)
    return date